/**
 * Maxymiser Core - Request (Media Fork)
 *
 * A handy wrap around `mmcore.CGRequest`.
 *
 * @version 0.4.0
 *
 * @requires mmcore.Deferred
 * @requires mmcore.tryCatch
 *
 * @author evgeniy.pavlyuk@maxymiser.com (Evgeniy Pavlyuk)
 */
(function() {
  'use strict';
  var ACTION_TRACKING_PAGE_ID = 'mmevents';
  var SECOND = 1000;
  var REQUEST_TIMEOUT = 6 * SECOND;
  var followingRequest;
  var prepareFollowingRequest = function() {
    followingRequest = mmcore.Deferred();
    mmcore.request.promise = followingRequest.promise();
  };
  /**
   * @param {string=} pageId If not sepcified, `event` is asssumed.
   * @param {boolean=} isSynchronous If not specified, the request will be asynchronous.
   * @return {!Object} Promise.
   */
  mmcore.request = function(pageId, isSynchronous, timeout) {
    var request = followingRequest;
    var secondArg = isSynchronous;
    var localTimeout = timeout || REQUEST_TIMEOUT;
    prepareFollowingRequest();
    if (!arguments.length) {
      pageId = ACTION_TRACKING_PAGE_ID;
    }
    mmcore.SetPageID(pageId);
    if (typeof secondArg === 'object') {
      // Keep backward compatibility.
      switch(true) {
        case ('sync' in secondArg):
          mmcore._async = !secondArg.sync; 
          break;
        case ('isSynchronous' in secondArg):
          mmcore._async = !secondArg.isSynchronous; 
          break;
        case ('async' in secondArg):
          mmcore._async = !!secondArg.async; 
          break;
        default: 
          mmcore._async = true; 
          break;
      }      
      localTimeout = secondArg.timeout || localTimeout;
    } else {
      mmcore._async = !isSynchronous;
    }
    mmcore.CGRequest(request.resolve);  // Callback is called in a try block in a response.
    setTimeout(mmcore.tryCatch(request.reject), localTimeout);
    return request.promise();
  };
  prepareFollowingRequest();
}());