> Media team's fork, fixing time of waiting response.
> **Note**: master (jQuery) branch is no more supported.

Maxymiser Core - Request
========================

A handy wrap around `mmcore.CGRequest`.

Description
-----------

A request to the content generator is often made to track an action:

```javascript
// Track an action.
mmcore.SetAction('T1Click', 1);
mmcore.SetPageID('event');
mmcore._async = true;  // Actions are normally tracked asynchronously.
mmcore.CGRequest();
```
Or to generate a campaign (by requesting the page the campaign element is mapped to):

```javascript
// Generate a campaign.
mmcore.SetPageID('t1-test-page');
mmcore._async = true;  // Or false - very rarely - if required.
mmcore.CGRequest();
```

`mmcore.request` is easier in use, and provides additinal functionality (that is described later on). Let us do the actions described above using it.

No arguments are required at all to track an action:

```javascript
// Track an action.
mmcore.SetAction('T1Click', 1);
mmcore.request();
```

If `mmcore.request` is called without arguments, the page ID `event` is used, and the request is made asynchronously. You no longer need to specify a page ID and mark a request as asynchronous when you need to track an action.

To generate a campaign you just need to specify its page ID:

```javascript
// Generate a campaign (asynchronously).
mmcore.request('t1-test-page');
```

The page `t1-test-page` will be asynchronously requested. If a situation requires a request to be made synchronously, you just need to pass `true` as the second argument:

```javascript
// Generate a campaign synchronously.
mmcore.request('t1', /* Is synchronous? */ true);
```

It is possible to set custom time for waitin for response. Default timeout is 6 seconds.
```javascript
mmcore.SetAction('T1_BarrierClick', 1);
// If response time will take more than 3 seconds then execute onFail() function.
// We assign 2nd parameter as a false here because request is asynchronous.
mmcore.request('mmevents', false, 3 * 1000)
    .done(onDone)
    .fail(onFail);
```

Furthermore, `mmcore.request` returns a jQuery promise object. So you can easily write a code like this:

```javascript
campaign.hideContent();
mmcore.request('t1-test-page')
    .done(campaign.renderMaxyboxes)
    .always(campaign.showContent);
```

It is possible to add handlers for the following request even before the request is done:

```javascript
var promiseBeforeRequest, promiseReturnedByRequest;

promiseBeforeRequest = mmcore.request.promise
    .done(function() {
      // Successful response.
    })
    .fail(function() {
      // Response time out, syntax erroor or return call from a site/campaign script in the response.
    })
    .always(function() {
      // Anyway.
    });

promiseReturnedByRequest = mmcore.request()
    .done(function() {
      // Successful response.
    })
    .fail(function() {
      // Response time out, syntax erroor or return call from a site/campaign script in the response.
    })
    .always(function() {
      // Anyway.
    }

promiseBeforeRequest === promiseReturnedByRequest;  // true.

// Since the request has been made, a promise to the following one becomes available.
mmcore.request.promise !=== promiseReturnedByRequest;  // true.
```

Installation
------------

The project is a part of [Maxymiser Core - Essentials](https://bitbucket.org/gtsmediateam/essentials-all) - it is installed together with the other projects.

If you want to install this project only, then:

1. Create a site script, and put the contents of [request.min.js](http://gitlab.maxymiser.net/gts/mmcore-request/raw/master/src/request.min.js) there.
2. Create an overlay page that covers all the site pages. Most likely it has already been created.
3. Map the script with the lowest order (it must be executed before other scripts, so they can immediately use it) to the page.

Dependencies
------------

* [Maxymiser Core - Deferred](https://bitbucket.org/gamingteam/essentials-deferred)
* [Maxymiser Core - Try Catch](https://bitbucket.org/gamingteam/essentials-trycatch)
